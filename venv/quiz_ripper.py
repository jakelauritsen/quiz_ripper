import re

def main():
    # with open('/home/jakelauritsen/School/fall_2020/cs3100/quizzes/jakes/eriks_question_pool.txt', 'r') as ref_file:
    #     with open('/home/jakelauritsen/School/fall_2020/cs3100/quizzes/jakes/stripped_pool.txt', 'a') as new_file:
    #         # for line in ref_file:
    #         line = ref_file.readline()
    #         line = ref_file.readline()
    #         line = ref_file.readline()
    #         line = ref_file.readline()
    line = "<link rel=\"stylesheet\" href=\"https://instructure-uploads-2.s3.amazonaws.com/account_10090000000000015/attachments/62932099/usu_design_tools_app.css?AWSAccessKeyId=AKIAJFNFXH2V2O7RPCAA&amp;Expires=1949581563&amp;Signature=zBfLf%2Bsc6EVsEt7kCkYdKnNxhMY%3D&amp;response-cache-control=Cache-Control%3Amax-age%3D473364000.0%2C%20public&amp;response-expires=473364000.0\"><p>This quiz covers material from Chapter 1 of the book.  You may use your notes and book, but you can not work with anyone else during the quiz.  All questions can assumed to mean, \"According to the book or as discussed in class...\"</p><script src=\"https://instructure-uploads-2.s3.amazonaws.com/account_10090000000000015/attachments/64592351/canvas_global_app.js\"></script>"

    print("String of Interest: ")
    print(line + '\n')

    script_tag_regex = re.compile(r'<script(\w|\W)*</script>')
    link_tag_regex = re.compile(r'<link(\w|\W)*>')
    print("After script tag removal: ")
    line = script_tag_regex.sub('', line)
    print(line + '\n')
    print()
    print("After link tag removal: ")
    line = link_tag_regex.sub('', line)
    print(line)


if __name__ == '__main__':
    main()
